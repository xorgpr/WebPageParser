﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPageParser.models
{
    public enum HtmlWebPageStatus
    {
        New,
        Working,
        Parsed,
    }

    public class HtmlWebPage
    {
        public HtmlWebPage()
        {

        }

        public HtmlWebPage(HtmlWebPageStatus status, int level)
        {
            Status = status;
            Level = level;
        }

        public HtmlWebPageStatus Status { get; set; }
        public int Level { get; set; }
        public List<string> Links { get; set; }
        public string StringHtml { get; set; }
        public string CurrentLink { get; set; }
        public int ResultSearced { get; set; }
    }
}
