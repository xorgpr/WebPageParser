﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPageParser.models;

namespace WebPageParser.BL
{
    public interface IParseHtmlPage
    {
        HtmlWebPage ParseHtmlPage(string htmlPage, int level, string url, string searchString);
    }
}
