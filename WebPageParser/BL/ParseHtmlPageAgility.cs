﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WebPageParser.models;

namespace WebPageParser.BL
{
    public class ParseHtmlPageAgility : IParseHtmlPage
    {
        public int Level {get; private set;}
        public string SearchString { get; set; }
        public int ResultSearced { get; set; }

        public HtmlWebPage ParseHtmlPage(string htmlPage, int level, string url, string searchString)
        {
            Level = level + 1;
            SearchString = searchString;
            ResultSearced = 0;

            var doc = new HtmlDocument();
            doc.LoadHtml(htmlPage);

            List<string> allLinks = null;
            try
            {
                allLinks = doc.DocumentNode.SelectNodes("//a[@href]").Select(l => l.GetAttributeValue("href", null)).Where(i => i.ToLower().StartsWith("http://".ToLower()) || i.ToLower().StartsWith("https://".ToLower())).Distinct().ToList();
            }
            catch(Exception ex)
            {

            }

            string textHtml = null;
            try
            {
                textHtml = doc.DocumentNode.SelectNodes("//text()").Where(l => l.ParentNode.Name.ToLower() != "script".ToLower()).Select(t => t.InnerText).Aggregate((i, j) => i + j);
            }
            catch(Exception ex)
            {

            }
            if (textHtml != null)
            {
                ResultSearced = Regex.Matches(textHtml, SearchString).Count;
            }

            return new HtmlWebPage() { Level = this.Level, Links = allLinks, StringHtml = textHtml, CurrentLink = url, ResultSearced = this.ResultSearced };
        }
    }
}
