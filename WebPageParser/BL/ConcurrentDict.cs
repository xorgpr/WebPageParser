﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPageParser.models;

namespace WebPageParser.BL
{
    public class ConcurrentDict
    {
        private static ConcurrentDictionary<string, HtmlWebPage> localQueue = new ConcurrentDictionary<string, HtmlWebPage>();
        private static object locker = new object();

        public ConcurrentDict()
        {

        }

        public string ShowResult()
        {
            return localQueue.Where(i => i.Value.Status == HtmlWebPageStatus.Parsed).OrderBy(i => i.Value.Level).Select(k =>
                new { Link = k.Key, k.Value.Level, LinkCount = k.Value.Links?.Count, k.Value.ResultSearced }.ToString()
                ).Aggregate((j, k) => j + "\n\r" + k);
        }

        public bool IsAnyForParse()
        {
            lock(locker)
            {
                return localQueue.Any(d => d.Value.Status == HtmlWebPageStatus.New || d.Value.Status == HtmlWebPageStatus.Working);
            }
        }

        public void Clear()
        {
            lock(locker)
            {
                localQueue.Clear();
            }
        }

        public void Add(string key, HtmlWebPage value)
        {
            lock(locker)
            {
                localQueue.GetOrAdd(key, value);
            }
        }

        public KeyValuePair<string, HtmlWebPage> GetNewAndSetWorking()
        {
            lock(locker)
            {
                var firstNew = localQueue.Where(d => d.Value.Status == HtmlWebPageStatus.New).OrderBy(d => d.Value.Level).FirstOrDefault();

                if (firstNew.Value != null)
                {
                    firstNew.Value.Status = HtmlWebPageStatus.Working;
                }

                return firstNew;
            }
        }

        internal void ResultForParsing(string key, HtmlWebPage htmlWebPage)
        {
            lock(locker)
            {
                HtmlWebPage value;

                var result = localQueue.TryGetValue(key, out value);

                if (result)
                {
                    value.Level = htmlWebPage.Level;
                    value.Links = htmlWebPage.Links;
                    value.Status = HtmlWebPageStatus.Parsed;
                    value.StringHtml = htmlWebPage.StringHtml;
                    value.CurrentLink = key;
                    value.ResultSearced = htmlWebPage.ResultSearced;

                    if (htmlWebPage.Links == null)
                        return;

                    foreach(string link in htmlWebPage.Links)
                    {
                        localQueue.GetOrAdd(link, new HtmlWebPage(HtmlWebPageStatus.New, htmlWebPage.Level + 1));
                    }
                }
            }
        }
    }
}
