﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebPageParser.BL
{
    public interface IGetHtmlPage
    {
        string GetStringFromHtml(string uri);
    }
}
