﻿using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WebPageParser.BL
{
    public class GetHtmlPageSelenium : IGetHtmlPage
    {
        ChromeDriver driver = null;

        private void InitDriver()
        {
            driver = new ChromeDriver(Environment.CurrentDirectory + @"\driver\");
            driver.Manage().Window.Maximize();
        }

        public GetHtmlPageSelenium()
        {
            InitDriver();
        }

        public string GetStringFromHtml(string uri)
        {
            if (driver == null)
                InitDriver();

            Thread.Sleep(100);

            driver.Navigate().GoToUrl(uri);

            Thread.Sleep(7000);

            return driver.PageSource;
        }
    }
}
