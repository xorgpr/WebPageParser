﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WebPageParser.models;

namespace WebPageParser.BL
{
    public class WebPageParser
    {
        private ConcurrentDict findQueue = new ConcurrentDict();
        int realCount = 0;
        public int Progress { get; private set; }
        private const int timesForWaitingTask = 5;

        public void Parse(string startUrl, int threadsCount, string searchText, int scanUrlCount)
        {
            int howManyParse = scanUrlCount;
            realCount = 0;

            findQueue.Clear();
            findQueue.Add(startUrl, new HtmlWebPage(HtmlWebPageStatus.New, 0));

            Progress = 0;
            if (UpdateProgress != null)
                UpdateProgress(this, EventArgs.Empty);

            for (int i = 0; i < threadsCount; ++i)
            {
                ThreadPool.QueueUserWorkItem((state) =>
                {
                    IGetHtmlPage getHtmlPage = new GetHtmlPageWebClient();
                    IParseHtmlPage parseHtmlPage = new ParseHtmlPageAgility();

                    for (int k = 0; k < timesForWaitingTask;)
                    {
                        if (findQueue.IsAnyForParse())
                        {
                            k = 0;

                            var firstNewElement = findQueue.GetNewAndSetWorking();
                            if (firstNewElement.Key != null)
                            {
                                ++realCount;

                                var procentProgress = ((double)realCount / (double)howManyParse) * 100.0;

                                procentProgress = procentProgress >= 100 ? 100.0 : procentProgress;

                                Progress = (int)procentProgress;

                                if (UpdateProgress != null)
                                    UpdateProgress(this, EventArgs.Empty);

                                if (realCount > howManyParse)
                                    return;

                                string innerHtml = getHtmlPage.GetStringFromHtml(firstNewElement.Key);
                                HtmlWebPage htmlWebPage = parseHtmlPage.ParseHtmlPage(innerHtml, firstNewElement.Value.Level, firstNewElement.Key, searchText);

                                findQueue.ResultForParsing(firstNewElement.Key, htmlWebPage);
                            }
                        }
                        else
                        {
                            Thread.Sleep(100);

                            ++k;
                        }
                    }
                }, null);
            }        
        }

        public event EventHandler UpdateProgress;
    }
}
