﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WebPageParser.BL
{
    public class GetHtmlPageWebClient : IGetHtmlPage
    {
        WebClient client = null;

        private void InitClient()
        {
            client = new WebClient();
        }

        public GetHtmlPageWebClient()
        {
            InitClient();
        }

        public string GetStringFromHtml(string uri)
        {
            if (client == null)
                InitClient();
            try
            {
                return client.DownloadString(uri);
            }
            catch(Exception ex)
            {
                return string.Empty;
            }
        }
    }
}
