﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPageParser.models;
using System.Text.RegularExpressions;

namespace WebPageParser.BL
{
    public class ParseHtmlPageRegExp : IParseHtmlPage
    {
        public int Level { get; private set; }
        public string SearchString { get; set; }

        public HtmlWebPage ParseHtmlPage(string htmlPage, int level, string url, string searchString)
        {
            Level = level + 1;
            SearchString = searchString;

            var allLinks = Regex.Matches(htmlPage, @"<a[^>]href=(['""])(https*:[^'""]+)\1").Cast<Match>().Select(r => r.Groups[2].Value).Distinct().ToList();
            var textHtml = Regex.Replace((Regex.Split(htmlPage, @"<[^<]*body[^>]*>")[1].ToString()), @"<[^>]*>", String.Empty);

            return new HtmlWebPage() { Level = this.Level, Links = allLinks, StringHtml = textHtml, CurrentLink = url };
        }
    }
}
