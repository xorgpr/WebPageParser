﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebPageParser.BL;

namespace WebPageParser
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //BL.WebPageParser webPageParser = new BL.WebPageParser();
            //webPageParser.UpdateProgress += (e, p) => 
            //{
            //    int abc = 4;
            //};
            //webPageParser.Parse(null, 1, null, 3);

            Application.Run(new MainForm());
        }
    }
}
