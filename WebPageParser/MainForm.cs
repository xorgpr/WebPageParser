﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;
using WebPageParser.BL;
using WebPageParser.models;

namespace WebPageParser
{
    public partial class MainForm : Form
    {
        private ConcurrentDict findQueue = new ConcurrentDict();
        int realCount = 0;

        public MainForm()
        {
            InitializeComponent();
        }

        private void buttonStartParse_Click(object sender, EventArgs e)
        {
            int howManyParse = (int)numericUpDownSetHowManyParse.Value;
            realCount = 0;

            findQueue.Clear();
            findQueue.Add(textBoxFirstLink.Text, new HtmlWebPage(HtmlWebPageStatus.New, 0));

            progressBarProgress.Value = 0;
            
            for (int i = 0; i < (int)numericUpDownThreads.Value; ++i)
            {
                ThreadPool.QueueUserWorkItem((state) =>
                {
                    IGetHtmlPage getHtmlPage = new GetHtmlPageWebClient();
                    IParseHtmlPage parseHtmlPage = new ParseHtmlPageAgility();

                    for (int k = 0; k < 5;)
                    {
                        if (findQueue.IsAnyForParse())
                        {
                            k = 0;
                            
                            var firstNewElement = findQueue.GetNewAndSetWorking();
                            if (firstNewElement.Key != null)
                            {
                                ++realCount;

                                var procentProgress = ((double)realCount / (double)howManyParse) * 100.0;

                                procentProgress = procentProgress >= 100 ? 100.0 : procentProgress;

                                progressBarProgress.BeginInvoke((Action)delegate () {
                                    progressBarProgress.Value = (int)procentProgress;
                                });

                                labelResultCount.BeginInvoke
                                    ((Action)delegate () {
                                        labelResultCount.Text = realCount.ToString();
                                    });

                                if (realCount > howManyParse)
                                    return;

                                string innerHtml = getHtmlPage.GetStringFromHtml(firstNewElement.Key);
                                HtmlWebPage htmlWebPage = parseHtmlPage.ParseHtmlPage(innerHtml, firstNewElement.Value.Level, firstNewElement.Key, textBoxSearchString.Text);
                                
                                findQueue.ResultForParsing(firstNewElement.Key, htmlWebPage);
                            }
                        }
                        else
                        {
                            Thread.Sleep(100);

                            ++k;
                        }
                    }
                }, null);
            }
        }

        private void buttonShowRes_Click(object sender, EventArgs e)
        {
            richTextBoxResult.Text = findQueue.ShowResult();
        }
    }
}
