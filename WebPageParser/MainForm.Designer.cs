﻿namespace WebPageParser
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxFirstLink = new System.Windows.Forms.TextBox();
            this.buttonStartParse = new System.Windows.Forms.Button();
            this.numericUpDownThreads = new System.Windows.Forms.NumericUpDown();
            this.textBoxSearchString = new System.Windows.Forms.TextBox();
            this.labelResultCount = new System.Windows.Forms.Label();
            this.richTextBoxResult = new System.Windows.Forms.RichTextBox();
            this.buttonShowRes = new System.Windows.Forms.Button();
            this.numericUpDownSetHowManyParse = new System.Windows.Forms.NumericUpDown();
            this.progressBarProgress = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownThreads)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSetHowManyParse)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxFirstLink
            // 
            this.textBoxFirstLink.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFirstLink.Location = new System.Drawing.Point(144, 44);
            this.textBoxFirstLink.Name = "textBoxFirstLink";
            this.textBoxFirstLink.Size = new System.Drawing.Size(526, 22);
            this.textBoxFirstLink.TabIndex = 0;
            this.textBoxFirstLink.Text = "https://google.com";
            // 
            // buttonStartParse
            // 
            this.buttonStartParse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonStartParse.Location = new System.Drawing.Point(676, 34);
            this.buttonStartParse.Name = "buttonStartParse";
            this.buttonStartParse.Size = new System.Drawing.Size(99, 42);
            this.buttonStartParse.TabIndex = 1;
            this.buttonStartParse.Text = "Start Parse";
            this.buttonStartParse.UseVisualStyleBackColor = true;
            this.buttonStartParse.Click += new System.EventHandler(this.buttonStartParse_Click);
            // 
            // numericUpDownThreads
            // 
            this.numericUpDownThreads.Location = new System.Drawing.Point(18, 45);
            this.numericUpDownThreads.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownThreads.Name = "numericUpDownThreads";
            this.numericUpDownThreads.Size = new System.Drawing.Size(120, 22);
            this.numericUpDownThreads.TabIndex = 2;
            this.numericUpDownThreads.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // textBoxSearchString
            // 
            this.textBoxSearchString.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSearchString.Location = new System.Drawing.Point(144, 110);
            this.textBoxSearchString.Name = "textBoxSearchString";
            this.textBoxSearchString.Size = new System.Drawing.Size(526, 22);
            this.textBoxSearchString.TabIndex = 3;
            this.textBoxSearchString.Text = "and";
            // 
            // labelResultCount
            // 
            this.labelResultCount.AutoSize = true;
            this.labelResultCount.Location = new System.Drawing.Point(713, 145);
            this.labelResultCount.Name = "labelResultCount";
            this.labelResultCount.Size = new System.Drawing.Size(16, 17);
            this.labelResultCount.TabIndex = 4;
            this.labelResultCount.Text = "0";
            // 
            // richTextBoxResult
            // 
            this.richTextBoxResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxResult.Location = new System.Drawing.Point(18, 178);
            this.richTextBoxResult.Name = "richTextBoxResult";
            this.richTextBoxResult.Size = new System.Drawing.Size(757, 247);
            this.richTextBoxResult.TabIndex = 5;
            this.richTextBoxResult.Text = "";
            // 
            // buttonShowRes
            // 
            this.buttonShowRes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonShowRes.Location = new System.Drawing.Point(676, 103);
            this.buttonShowRes.Name = "buttonShowRes";
            this.buttonShowRes.Size = new System.Drawing.Size(99, 34);
            this.buttonShowRes.TabIndex = 6;
            this.buttonShowRes.Text = "Show Result";
            this.buttonShowRes.UseVisualStyleBackColor = true;
            this.buttonShowRes.Click += new System.EventHandler(this.buttonShowRes_Click);
            // 
            // numericUpDownSetHowManyParse
            // 
            this.numericUpDownSetHowManyParse.Location = new System.Drawing.Point(18, 111);
            this.numericUpDownSetHowManyParse.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericUpDownSetHowManyParse.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownSetHowManyParse.Name = "numericUpDownSetHowManyParse";
            this.numericUpDownSetHowManyParse.Size = new System.Drawing.Size(120, 22);
            this.numericUpDownSetHowManyParse.TabIndex = 7;
            this.numericUpDownSetHowManyParse.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // progressBarProgress
            // 
            this.progressBarProgress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBarProgress.Location = new System.Drawing.Point(18, 139);
            this.progressBarProgress.Name = "progressBarProgress";
            this.progressBarProgress.Size = new System.Drawing.Size(652, 23);
            this.progressBarProgress.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 17);
            this.label1.TabIndex = 9;
            this.label1.Text = "Threads:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(141, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "First URL:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(141, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "Find word:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 17);
            this.label4.TabIndex = 12;
            this.label4.Text = "Pages parse:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.progressBarProgress);
            this.Controls.Add(this.numericUpDownSetHowManyParse);
            this.Controls.Add(this.buttonShowRes);
            this.Controls.Add(this.richTextBoxResult);
            this.Controls.Add(this.labelResultCount);
            this.Controls.Add(this.textBoxSearchString);
            this.Controls.Add(this.numericUpDownThreads);
            this.Controls.Add(this.buttonStartParse);
            this.Controls.Add(this.textBoxFirstLink);
            this.Name = "MainForm";
            this.Text = "Web Page Parser";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownThreads)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSetHowManyParse)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxFirstLink;
        private System.Windows.Forms.Button buttonStartParse;
        private System.Windows.Forms.NumericUpDown numericUpDownThreads;
        private System.Windows.Forms.TextBox textBoxSearchString;
        private System.Windows.Forms.Label labelResultCount;
        private System.Windows.Forms.RichTextBox richTextBoxResult;
        private System.Windows.Forms.Button buttonShowRes;
        private System.Windows.Forms.NumericUpDown numericUpDownSetHowManyParse;
        private System.Windows.Forms.ProgressBar progressBarProgress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

