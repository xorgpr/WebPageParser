﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPageParser.BL;
using Xunit;

namespace WebPageParser.Tests.BL
{
    public class GetHtmlPage
    {
        [Fact]
        public void GetStringFromHtml()
        {
            var mock = new Mock<IGetHtmlPage>();
            mock.Setup(m => m.GetStringFromHtml(It.IsAny<string>())).Returns("<html></html>");

            var result = mock.Object.GetStringFromHtml("url");

            Assert.Equal("<html></html>", result);
        }
    }
}
