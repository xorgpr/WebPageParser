﻿using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebPageParser.BL;
using Xunit;

namespace WebPageParser.Tests.BL
{
    public class ParseHtmlPage
    {
        [Fact]
        public void ParseHtmlPageTest()
        {
            var mock = new Mock<IParseHtmlPage>();
            mock.Setup(m => m.ParseHtmlPage(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>())).Returns(new models.HtmlWebPage()
            {
                CurrentLink = "https://curlink.com",
                Level = 1,
                Links = new List<string>() { "l1", "l2", "l3" },
                StringHtml = "<html></html>"
            });

            var result = mock.Object.ParseHtmlPage("a1", 3, "d2", "s3");

            Assert.Equal(3, result.Links.Count);
        }
    }
}
